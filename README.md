# Learn ES6

## 获取

``` bash
$ git clone https://git.oschina.net/esdemo/learn-es6.git
```

## 运行

``` bash
$ npm install
$ webpack --progress --watch
```