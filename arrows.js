var nums = [1, 2, 3, 4, 5];

nums.forEach((v, i) => {
    console.log('value:', v, ',index:', i);
});

var strings = nums.map(v => v.toString());
console.log('strings:', strings);

var objs = nums.map((v, i) => ({ value: v, index: i }));
console.log('objs:', objs);