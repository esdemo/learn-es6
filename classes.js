"use strict";

class Person {
    constructor(name) {
        this.name = name;
    }
    SayHello() {
        console.log('Hello,', this.name);
    }
}

class Game extends Person {
    constructor(name) {
        super(name)
    }
    Play() {
        console.log(this.name, ' is playing a game.');
    }
}

class Foo {
    static Bar() {
        console.log('Foo=Bar');
    }
}

let person = new Person('Lyric');
person.SayHello();

let game = new Game('Elva');
game.SayHello();
game.Play();

Foo.Bar();