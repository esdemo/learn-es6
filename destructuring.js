let [a, , b] = [1, 2, 3];

console.log(a, b);

var foo = function () {
    return {
        Bar: 'Bar'
    };
}

let {Bar: B} = foo();
console.log(B);

function hello({name: x}) {
    console.log(`Hello,${x}`);
}
hello({ name: 'Lyric' });