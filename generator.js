function* helloworldGenerator() {
    yield "hello";
    yield "world";
}

function* fooGenerator() {
    yield "bar";
    yield* helloworldGenerator();
}

for (let v of fooGenerator()) {
    console.log('v:', v);
}