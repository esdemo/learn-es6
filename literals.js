"use strict";

function handler() {
    console.log('handler');
}

var obj = {
    handler,
    toString() {
        console.log('obj');
    },
    ['prop_' + (() => 42)()]: 42
};

console.log(obj['prop_42']);
obj.handler();
obj.toString();