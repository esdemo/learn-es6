class Person {
    constructor(name) {
        this.name = name;
    }
    hello() {
        return 'Hello,' + this.name;
    }
    static createPerson(name) {
        return new Person(name);
    }
}

export {Person};