let timeout = function (ms = 100) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, ms, Date.now());
    })
};

timeout().then(v => {
    console.log(v);
});

console.log('Start...');