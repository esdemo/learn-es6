let util = require('util');

function sum(a, b = 12) {
    return a + b;
}

console.log('Sum:', sum(3));

function summary(a, ...b) {
    let val = Number(a);
    b.forEach(v => {
        val += v;
    });
    return val;
}

console.log('Summary:', summary(1, 2, 3, 4, 5, 6));

function rest(x, y, z) {
    return util.format('x:%d,y:%d,z:%d', x, y, z);
}

console.log(rest(...[1, 2, 3]));