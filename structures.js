let set = new Set(['foo']);
set.add('hello').add('hello').add('world');

set.forEach(v => {
    console.log(v);
});

let map = new Map([['foo', 'bar']]);

console.log('foo:', map.get('foo'));