module.exports = {
    entry: {
        destructuring: './destructuring.js',
        spread: './spread.js',
        generator: ['babel-polyfill', './generator.js'],
        module: './modules/index.js',
        structures: ['babel-polyfill', './structures.js'],
        promise: ['babel-polyfill', './promise.js']
    },
    output: {
        filename: '[name].bundle.js',
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',
        }]
    }
};